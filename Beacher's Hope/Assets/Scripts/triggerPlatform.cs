using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triggerPlatform : MonoBehaviour
{
    platformMovement platform;
    
    private void Start()
    {
        platform = GetComponent<platformMovement>();
    }

    private void OnTriggerEnter(Collider other)
    {
        platform.canMove = true;
    }
}
