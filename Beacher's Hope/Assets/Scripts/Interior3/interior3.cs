using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class interior3 : MonoBehaviour
{
    public Animator Puerta;

    private void OnTriggerEnter(Collider other)
    {
        Puerta.Play("puertaInterior_3");
    }
    private void OnTriggerExit(Collider other)
    {
        Puerta.Play("puertaInterior_3cerrar");
    }
}
