using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puerta2 : MonoBehaviour
{
    public Animator Puerta;

    private void OnTriggerEnter(Collider other)
    {
        Puerta.Play("puerta_2");
    }
    private void OnTriggerExit(Collider other)
    {
        Puerta.Play("puerta_2cerrar");
    }
}
